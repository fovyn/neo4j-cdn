CREATE DATABASE adventureWorks IF NOT EXISTS;
MATCH (n) DETACH DELETE n

// LOAD PRODUCTS
LOAD CSV WITH HEADERS FROM 'https://gitlab.com/fovyn/neo4j-cdn/-/raw/main/adventureworks/products.csv?ref_type=heads' as product FIELDTERMINATOR ','
MERGE (p:Product { productID: product.ProductID, productNumber: product.ProductNumber, name: product.ProductName, standardCost: product.StandardCost, listPrice: product.ListPrice, subCategoryID: product.SubCategoryID})
MERGE (m:Model { modelName: product.ModelName})
MERGE (p)-[:PART_OF]->(m)

// LOAD CATEGORIES
LOAD CSV WITH HEADERS FROM 'https://gitlab.com/fovyn/neo4j-cdn/-/raw/main/adventureworks/productcategories.csv?ref_type=heads' as productcategories FIELDTERMINATOR ','
MERGE (c:Category { categoryID: productcategories.CategoryID, name: productcategories.CategoryName })

LOAD CSV WITH HEADERS FROM 'https://gitlab.com/fovyn/neo4j-cdn/-/raw/main/adventureworks/productsubcategories.csv?ref_type=heads' as productsubcategories FIELDTERMINATOR ','
MATCH (c:Category { categoryID: productsubcategories.CategoryID })
MATCH (p:Product { subCategoryID: productsubcategories.SubCategoryID })
MERGE (sc:SubCategory{ subCategoryID: productsubcategories.SubCategoryID, name: productsubcategories.SubCategoryName })
MERGE (sc)-[:IS_SUB]-(c)
MERGE (p)-[:TYPE_OF]-(sc)

// LOAD CUSTOMERS
LOAD CSV WITH HEADERS FROM 'https://gitlab.com/fovyn/neo4j-cdn/-/raw/main/adventureworks/customers.csv?ref_type=heads' as customer FIELDTERMINATOR ','
MERGE (c:Customer { customerID: customer.CustomerID, firstName: customer.FirstName, lastName: customer.LastName })

// LOAD EMPLOYEES
LOAD CSV WITH HEADERS FROM 'https://gitlab.com/fovyn/neo4j-cdn/-/raw/main/adventureworks/employees.csv?ref_type=heads' as employee FIELDTERMINATOR ','
MERGE (e:Employee{ employeeID: employee.EmployeeID, firstName: employee.FirstName, lastName: employee.LastName, job: employee.JobTitle, organizationLevel: employee.OrganizationLevel, maritalStatus: employee.MaritalStatus, gender: employee.Gender, managerID: CASE employee.ManagerID WHEN 'NULL'THEN -1 ELSE employee.ManagerID END })
MERGE (l:Location { territory: employee.Territory, country: employee.Country, group: employee.Group })
MERGE (e)-[:LIVE_IN]-(l)

MATCH (e:Employee)
WHERE e.managerID <> -1
WITH e as Subordonate, e.managerID as managerID
MATCH (e:Employee {employeeID: managerID })
MERGE (Subordonate)-[:IS_MANAGE_BY]-(e)

// LOAD ORDERS
LOAD CSV WITH HEADERS FROM 'https://gitlab.com/fovyn/neo4j-cdn/-/raw/main/adventureworks/orders.csv?ref_type=heads' as order FIELDTERMINATOR ','
MATCH (e:Employee {employeeID: order.EmployeeID })
MATCH (c:Customer {customerID: order.CustomerID })
MATCH (p:Product { productID: order.ProductID })
MERGE (o:Order { orderID: order.SalesOrderID, orderDate: order.OrderDate, dueDate: order.DueDate, shipDate: order.ShipDate, subTotal: order.SubTotal, taxAmt: order.TaxAmt, freight: order.Freight, totalDue: order.TotalDue })
MERGE (o)-[:CONTAINS { orderQty: order.OrderQty, unitPrice: order.UnitPrice, unitPriceDiscount: order.UnitPriceDiscount, lineTotal: order.LineTotal}]-(p)


// LOAD VENDORS
LOAD CSV WITH HEADERS FROM 'https://gitlab.com/fovyn/neo4j-cdn/-/raw/main/adventureworks/vendors.csv?ref_type=heads' as vendor FIELDTERMINATOR ','
MERGE (v:Vendor { vendorID: vendor.VendorID, name: vendor.VendorName, accountNumber: vendor.AccountNumber, creditRating: vendor.CreditRating, active: CASE vendor.ActiveFlag WHEN 1 THEN true ELSE false END})

// LOAD VENDOR_PRODUCT
LOAD CSV WITH HEADERS FROM 'https://gitlab.com/fovyn/neo4j-cdn/-/raw/main/adventureworks/vendorproduct.csv?ref_type=heads' as vendorproduct FIELDTERMINATOR ','
MATCH (p:Product { productID: vendorproduct.ProductID })
MATCH (v:Vendor { vendorID: vendorproduct.VendorID })
MERGE (v)-[:SELL]->(p)